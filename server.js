const http=require('http');
const fs=require('fs');
const url = require('url');

let server=http.createServer((req,res)=>{
    
    if (req.url == '/html') {

        fs.readFile('main.html', 'utf-8',(err, data)=>{
            if (err){
                res.writeHead(400, {'content-type': 'text/html'})
                res.write('<h1>No HTML File Found !!!</h1>')
                res.end()
    
            } else {
                res.writeHead(200, {'content-type': 'text/html'})
                res.write(data)
                res.end()
                
            }

        })

    } else if (req.url == "/json") {

        fs.readFile('data.json', 'utf-8',(err, data)=>{
            if (err){
                res.writeHead(400, {'content-type': 'text/html'})
                res.write('<h1>No JSON File Found !!!</h1>')
                res.end()
                
            } else {
                res.writeHead(200, {'content-type': 'text/json'})
                res.write(data)
            
                res.end()
                
            }

        })

    } else if (req.url == "/uuid") {

        fs.readFile('uuid.json', 'utf-8',(err, data)=>{
            if (err){
                res.writeHead(400, {'content-type': 'text/html'})
                res.write('<h1>No JSON File Found !!!</h1>')
                res.end()
                
            } else {
                res.writeHead(200, {'content-type': 'text/json'})
                //sudo blkid | grep UUID= | head -1 > uuid.json
                let uuidIndex=data.lastIndexOf('UUID');
                let uuidData=data.substring(uuidIndex,data.length-2);
                let uuidObject={uuid:uuidData.substring(6)}
                res.write(JSON.stringify(uuidObject));
                res.end()
                
            }

        })

    }
    else if (req.url.includes("/status/")) {
        let reqCode=parseInt(req.url.substring(8));
        let statusCode=http.STATUS_CODES;
        if(reqCode in statusCode){
            res.writeHead(reqCode, {'content-type': 'text/plain'})
            res.write(`${res.statusMessage}\n`)
            res.end(`${reqCode}`)
        }
        else{
            res.writeHead(400, {'content-type': 'text/plain'})
            res.write("status code is not valid")
            res.end();
        }

    } 
    else if (req.url.includes("/delay/")) {
        
        const delayTime = parseInt(req.url.substring(7))*1000;
        console.log(delayTime);
        res.writeHead(200, {'content-type': 'text/plain'})
        
        setTimeout(()=>{res.end(`Status Code ${res.statusCode}`)},delayTime)
    
        
    } 
    else {
        
        console.log('invalid address');
        res.end('Invalid request');
    }
})
server.listen(8000, () => {
    console.log(`Server running at port 8000`)
  })
